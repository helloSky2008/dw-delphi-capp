library dll_functions;

{ Important note about DLL memory management: ShareMem must be the
  first unit in your library's USES clause AND your project's (select
  Project-View Source) USES clause if your DLL exports any procedures or
  functions that pass strings as parameters or function results. This
  applies to all strings passed to and from your DLL--even those that
  are nested in records and classes. ShareMem is the interface unit to
  the BORLNDMM.DLL shared memory manager, which must be deployed along
  with your DLL. To avoid using BORLNDMM.DLL, pass string information
  using PChar or ShortString parameters. }

uses
  system,
  SysInit,
  SysUtils,
  Classes,
  Windows,
  Messages,
  Forms,
  Dialogs,
  DB,
  ADODB,
  ShellAPI,
  udlltransfer in '..\dll_interfaces\udlltransfer.pas',
  ufrmcomm_showmessage in '..\common\ufrmcomm_showmessage.pas' {frmcomm_showmessage},
  udllfuns_menzshouf in '..\dll_interfaces\udllfuns_menzshouf.pas',
  udllfuns_cxbb_menzsf in '..\dll_interfaces\udllfuns_cxbb_menzsf.pas',
  udllfuns_menzyisz in '..\dll_interfaces\udllfuns_menzyisz.pas',
  udllfuns_cxbb_menzyisz in '..\dll_interfaces\udllfuns_cxbb_menzyisz.pas',
  udllfuns_menzhusz in '..\dll_interfaces\udllfuns_menzhusz.pas',
  udllfuns_cxbb_menzhusz in '..\dll_interfaces\udllfuns_cxbb_menzhusz.pas',
  udllfuns_zhuyguanl in '..\dll_interfaces\udllfuns_zhuyguanl.pas',
  udllfuns_cxbb_zhuygl in '..\dll_interfaces\udllfuns_cxbb_zhuygl.pas',
  udllfuns_zhuyyisz in '..\dll_interfaces\udllfuns_zhuyyisz.pas',
  udllfuns_cxbb_zhuy_yisz in '..\dll_interfaces\udllfuns_cxbb_zhuy_yisz.pas',
  udllfuns_zhuyhusz in '..\dll_interfaces\udllfuns_zhuyhusz.pas',
  udllfuns_cxbb_zhuy_husz in '..\dll_interfaces\udllfuns_cxbb_zhuy_husz.pas',
  udllfuns_yijguanl in '..\dll_interfaces\udllfuns_yijguanl.pas',
  udllfuns_cxbb_yijgl in '..\dll_interfaces\udllfuns_cxbb_yijgl.pas',
  udllfuns_emr_word in '..\dll_interfaces\udllfuns_emr_word.pas',
  udllfuns_yaokuguanli in '..\dll_interfaces\udllfuns_yaokuguanli.pas',
  udllfuns_cxbb_yaok in '..\dll_interfaces\udllfuns_cxbb_yaok.pas',
  udllfuns_yaofangguanli in '..\dll_interfaces\udllfuns_yaofangguanli.pas',
  udllfuns_cxbb_yaof in '..\dll_interfaces\udllfuns_cxbb_yaof.pas',
  udllfuns_wuzikuguanli in '..\dll_interfaces\udllfuns_wuzikuguanli.pas',
  udllfuns_cxbb_wuzk in '..\dll_interfaces\udllfuns_cxbb_wuzk.pas',
  udllfuns_fapiaoguanl in '..\dll_interfaces\udllfuns_fapiaoguanl.pas',
  udllfuns_cxbb_fapgl in '..\dll_interfaces\udllfuns_cxbb_fapgl.pas',
  udllfuns_cxbb_yuanzhang in '..\dll_interfaces\udllfuns_cxbb_yuanzhang.pas',
  udllfuns_tbldatadict in '..\dll_interfaces\udllfuns_tbldatadict.pas',
  udllfuns_tblmanage in '..\dll_interfaces\udllfuns_tblmanage.pas',
  udllfuns_xitguanl in '..\dll_interfaces\udllfuns_xitguanl.pas',
  ufrmcomm_showdialog in '..\common\ufrmcomm_showdialog.pas' {frmcomm_showdialog};

{$R *.res}
    
var
  { dll的Application和screen临时存储,在dll卸载的时候释放资源
  }
  DLLApp: TApplication;
  DLLScreen: TScreen;
                       
function getDbLoingIngo(connectionstring : string;
    var dbserver, dbuser, dbpassword, dbdatabase : string) : Boolean;
var
  i, p : Integer;
  strupperconnstring :string;
begin
  Result := False;
  dbserver := '';
  dbuser := '';
  dbpassword := '';
  dbdatabase := '';    
  strupperconnstring := UpperCase(connectionstring);
  //Provider=SQLOLEDB.1;
  //Password=12345;
  //Persist Security Info=True;
  //User ID=sa;
  //Initial Catalog=uphis;
  //Data Source=.

  //get password
  i := Pos(UpperCase('Password='), strupperconnstring);
  if i = 0 then Exit;
  i := i + Length('Password=');
  for p:=i to Length(strupperconnstring) do
  begin
    if strupperconnstring[p] = ';' then
      Break;
  end;
  dbpassword := Copy(connectionstring, i, p-i);

  //get userid
  i := Pos(UpperCase('User ID='), strupperconnstring);
  if i = 0 then Exit;
  i := i + Length('User ID=');
  for p:=i to Length(strupperconnstring) do
  begin
    if strupperconnstring[p] = ';' then
      Break;
  end;
  dbuser := Copy(connectionstring, i, p-i);

  //get dbdatabase
  i := Pos(UpperCase('Initial Catalog='), strupperconnstring);
  if i = 0 then Exit;
  i := i + Length('Initial Catalog=');
  for p:=i to Length(strupperconnstring) do
  begin
    if strupperconnstring[p] = ';' then
      Break;
  end;
  dbdatabase := Copy(connectionstring, i, p-i);

  //get dbserver
  i := Pos(UpperCase('Data Source='), strupperconnstring);
  if i = 0 then Exit;  
  i := i + Length('Data Source=');
  for p:=i to Length(strupperconnstring) do
  begin
    if strupperconnstring[p] = ';' then
      Break;
  end;
  dbserver := Copy(connectionstring, i, p-i);

  Result := True;
end;

{ dll卸载时的资源恢复和释放
}
procedure DLLUnloadProc(Reason: Integer); 
begin
  if Reason = 0 then //DLL_PROCESS_DETACH
  begin
    Application := DLLApp; //恢复
    Screen := DLLScreen;
    // 关闭dll
    SendMessage(Application.Handle, WM_CLOSE, 0, 0);
    FreeLibrary(Application.Handle);
  end;
end;
       
   
//系统管理
function frmmain_menuitem_click_xitgl(dllparam : PDllParam; menuid : integer):integer;  
var
  dbserver, dbuser, dbpassword, dbdatabase : string;
  reportdesignapp_path : string;
begin
  case menuid of 
    //业务办理
    1001: //参数设置
    begin
      dll_xitgl_cansshez(dllparam);
    end;
    1002: //修改口令
    begin
      dll_xitgl_xiugkoul(dllparam);
    end;
    1003: //权限管理
    begin
      dll_xitgl_qingksj(dllparam);
    end;
    1004: //报表设计
    begin
      if not getDbLoingIngo(dllparam^.padoconn.ConnectionString, dbserver,
        dbuser, dbpassword, dbdatabase) then
      begin
        Exit;
      end;

      reportdesignapp_path := ExtractFilePath(application.ExeName) + '\' +
        'preportdesign.exe';
      ShellExecute(0,'open',PChar(reportdesignapp_path),
        PChar(dbserver + ' ' + dbuser + ' ' + dbpassword + ' ' +
          dbdatabase), '', SW_SHOW);
    end;
  end;
end;


//基础数据
function frmmain_menuitem_click_jicsj(dllparam : PDllParam; menuid : integer):integer;
begin
  case menuid of
    //业务办理
    1001: //科室设置
    begin
      dll_tbl_kes(dllparam);
    end;
    6001: //病区设置
    begin
      dll_tbl_bingq(dllparam);
    end;
    6002: //床位设置
    begin
      dll_tbl_chuangw(dllparam);
    end;
    6003: //药库设置
    begin
      dll_tbl_yaok(dllparam);
    end;
    6004: //药房设置
    begin
      dll_tbl_yaof(dllparam);
    end;
    6005: //物资库设置
    begin
      dll_tbl_wuzk(dllparam);
    end;
    6006: //医生设置
    begin
      dll_tbl_yis(dllparam);
    end;
    6007: //患者设置
    begin
      dll_tbl_huanz(dllparam);
    end;
    6008: //项目设置
    begin
      dll_tbl_xiangm(dllparam);
    end;
    6009: //物资设置
    begin
      dll_tbl_wuz(dllparam);
    end;
    6010: //套餐设置
    begin
      dll_tbl_taoc(dllparam);
    end;

    //单据查询

    //查询报表

  end;
end;


{ 主界面菜单项单击处理事件
}
function frmmain_menuitem_click(dllparam : PDllParam; menuid : integer) : integer; stdcall;
begin   
  //这个大函数根据menuid调度不同的实际处理函数
  
  //系统管理
  if ((menuid >=1001) and (menuid<1200)) then
  begin
    frmmain_menuitem_click_xitgl(dllparam, menuid);
  end
  //基础数据
  else if((menuid>=1200) and (menuid<1300)) then
  begin
    frmmain_menuitem_click_jicsj(dllparam, menuid);
  end;

  Result := 1;
end;
      
exports
  frmmain_menuitem_click;

begin     
  { 将dll的Application和Screen存储下来,在卸载的时候恢复
  }
  DLLApp := Application;
  DLLScreen := Screen;
  //dll相关函数处理,这里处理卸载时的资源释放
  DLLProc := @DLLUnloadProc;
end.
 