program pcsmsgtest;

uses
  Forms,
  umain in 'umain.pas' {Form1},
  ucsmsg in 'ucsmsg.pas',
  ubase in '..\..\lib\common\ubase.pas',
  utypes in '..\..\lib\common\utypes.pas',
  HzSpell in '..\..\lib\moudle\huoqupinyin\HzSpell.pas',
  ufrmcomm_showmessage in '..\..\dllmoudle\common\ufrmcomm_showmessage.pas' {frmcomm_showmessage},
  uencode in '..\..\lib\common\uencode.pas';

{$R *.res}

begin
  Application.Initialize;
  Application.CreateForm(TForm1, Form1);
  Application.CreateForm(Tfrmcomm_showmessage, frmcomm_showmessage);
  Application.Run;
end.
